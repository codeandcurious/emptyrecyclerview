package com.nextreality.emptyrecyclerviewsample.ui.adapter

import com.nextreality.emptyrecyclerviewsample.ui.adapter.generated.SampleAdapterGenerated

class SampleAdapter : SampleAdapterGenerated() {
	
	companion object {
	
		const val TAG = "SampleAdapter"
	}
}
