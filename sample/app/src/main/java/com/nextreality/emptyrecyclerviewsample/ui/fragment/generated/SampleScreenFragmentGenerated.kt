// Managed by geny -----------------------------------------------------------
// <auto-generated>
//		     This code was generated by Rui Milagaia's Geny Tool.
//		     Runtime Version: 1.2 
//
//		     Changes to this file may cause incorrect behavior and will be lost if
//		     the code is regenerated.
// </auto-generated>
// Managed by geny -----------------------------------------------------------

package com.nextreality.emptyrecyclerviewsample.ui.fragment.generated

import android.os.Bundle
import com.tandeminnovation.appbase.helper.BundleHelper
import com.tandeminnovation.appbase.helper.BusHelper
import com.tandeminnovation.appbase.extension.whenNotNull
import com.tandeminnovation.appbase.eventbus.event.OnErrorEvent
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import com.nextreality.emptyrecyclerviewsample.R
import com.nextreality.emptyrecyclerviewsample.base.FragmentBase
import com.nextreality.emptyrecyclerviewsample.ui.adapter.SampleAdapter

abstract class SampleScreenFragmentGenerated protected constructor() : FragmentBase() {
	
	protected lateinit var mAdapter: SampleAdapter
	
	//UI methods
	
	override val mainLayoutRes: Int
		get() = R.layout.fragment_sample_screen
	
	override fun setup(savedInstanceState: Bundle?) {
		
		super.setup(savedInstanceState)
		
		setupRecyclerView()
	}
	@Subscribe(threadMode = ThreadMode.MAIN)
	fun onErrorEvent(event: OnErrorEvent) {
		handleOnErrorEvent(event)
	}
	
	
	protected open fun setupRecyclerView() {
		
		mAdapter = SampleAdapter()
		mAdapter.setup()
		mAdapter.updateDataSet(mutableListOf())
	}
	
	companion object {
	
		private const val TAG = "SampleScreenFragmentGenerated"
	}
}
