package com.nextreality.emptyrecyclerviewsample.models

import com.google.gson.annotations.SerializedName

data class ApiResponse (

	@SerializedName("status") val status : String,
	@SerializedName("totalResults") val totalResults : Int,
	@SerializedName("articles") val articles : List<Articles>
)