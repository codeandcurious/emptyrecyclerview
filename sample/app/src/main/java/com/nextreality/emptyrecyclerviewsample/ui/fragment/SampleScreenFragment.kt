package com.nextreality.emptyrecyclerviewsample.ui.fragment

import android.os.Bundle
import android.view.MotionEvent
import androidx.core.content.ContextCompat
import androidx.recyclerview.selection.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nextreality.emptyrecyclerviewsample.R
import com.nextreality.emptyrecyclerviewsample.api.NewsAPI
import com.nextreality.emptyrecyclerviewsample.api.model.NewsModel
import com.nextreality.emptyrecyclerviewsample.ui.adapter.SampleAdapter
import com.nextreality.emptyrecyclerviewsample.ui.adapter.viewholder.NewsItemViewHolder
import com.nextreality.emptyrecyclerviewsample.ui.adapter.wrapper.NewsItemWrapper
import com.nextreality.emptyrecyclerviewsample.ui.fragment.generated.SampleScreenFragmentGenerated
import com.tandeminnovation.android.emptyrecyclerview.data.RecyclerTracker
import com.tandeminnovation.android.emptyrecyclerview.itemdecorator.LinearDividerDecoration
import kotlinx.android.synthetic.main.empty_recyclerview.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SampleScreenFragment : SampleScreenFragmentGenerated() {

    override fun setup(savedInstanceState: Bundle?) {
        super.setup(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        GlobalScope.launch(Dispatchers.IO) {

            delay(5000)
            mAdapter.updateDataSet((NewsAPI().getNews().data as? MutableList<NewsModel>)?.map { NewsItemWrapper(it) }?.toList() ?: listOf())
            GlobalScope.launch(Dispatchers.Main) {
                mAdapter.notifyDataSetChanged()
            }
        }
    }

    inner class NewsItemKeyProvider(private val adapter: SampleAdapter) : ItemKeyProvider<Long>(SCOPE_CACHED) {
        override fun getKey(position: Int) = (adapter.dataSet[position] as NewsItemWrapper).obj.id
        override fun getPosition(key: Long) = adapter.dataSet.indexOfFirst { (it as NewsItemWrapper).obj.id == key }
    }


    inner class ItemLookup(val rv: RecyclerView) : ItemDetailsLookup<Long>() {
        override fun getItemDetails(e: MotionEvent): ItemDetails<Long>? {
            val view = rv.findChildViewUnder(e.x, e.y)
            if (view != null) {
                return (rv.getChildViewHolder(view) as NewsItemViewHolder).getItemDetails()
            }
            return null
        }
    }

    override fun setupRecyclerView() {
        super.setupRecyclerView()

        recyclerView.apply {
            layoutManager = LinearLayoutManager(requireContext())
//			addItemDecoration(MarginDecoration(requireContext(), R.dimen.space_item_decoration_margin))
//            addItemDecoration(LinearDividerDecoration(ContextCompat.getColor(requireContext(), R.color.purple_200), 10, leftMargin = 24, rightMargin = 24))
            ContextCompat.getDrawable(requireContext(), R.drawable.ic_launcher_background)?.let { addItemDecoration(LinearDividerDecoration(it, 10)) }
            adapter = mAdapter
        }
        val tracker = RecyclerTracker(
            SelectionTracker.Builder(
                "longSelectionTracker",
                recyclerView,
                NewsItemKeyProvider(mAdapter),
                ItemLookup(recyclerView),
                StorageStrategy.createLongStorage()
            ).withSelectionPredicate(SelectionPredicates.createSelectSingleAnything())
                .build()
        )
        mAdapter.recyclerTracker = tracker
    }

    companion object {

        const val TAG = "SampleScreenFragment"

        /**
         * Use this factory method to create a new instance of this fragment using
         * the provided parameters.
         */
        fun newInstance(): SampleScreenFragment {

            return SampleScreenFragment()
        }
    }
}
