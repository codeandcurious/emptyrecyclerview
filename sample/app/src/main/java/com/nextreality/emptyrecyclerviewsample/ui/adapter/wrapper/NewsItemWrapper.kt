package com.nextreality.emptyrecyclerviewsample.ui.adapter.wrapper

import com.nextreality.emptyrecyclerviewsample.api.model.NewsModel
import com.nextreality.emptyrecyclerviewsample.models.Articles
import com.nextreality.emptyrecyclerviewsample.ui.adapter.wrapper.generated.NewsItemWrapperGenerated

class NewsItemWrapper(newsItem : NewsModel) : NewsItemWrapperGenerated(newsItem) {
	
	companion object {
	
		const val TAG = "NewsItemWrapper"
		
		const val getType: Int = TYPE
	}
}
