package com.nextreality.emptyrecyclerviewsample.base

import android.os.Bundle

import com.tandeminnovation.appbase.base.CommonFragmentBase
import com.tandeminnovation.appbase.eventbus.event.OnErrorEvent
import com.nextreality.emptyrecyclerviewsample.helper.NavigationHelper

/**
 * 
 * @author Geny
 *
 */
abstract class FragmentBase : CommonFragmentBase() {
	
	protected val navigationHelper: NavigationHelper = NavigationHelper.instance
	
	override val mainLayoutRes: Int
		get() = 0
	
	override fun setup(savedInstanceState: Bundle?) {
		
		savedInstanceState?.let { super.onSaveInstanceState(it) }
	}
	
	override fun handleOnErrorEvent(event: OnErrorEvent) {}
}
