package com.nextreality.emptyrecyclerviewsample.ui.adapter.viewholder

import android.view.MotionEvent
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.selection.SelectionTracker
import com.nextreality.emptyrecyclerviewsample.R
import com.nextreality.emptyrecyclerviewsample.api.model.NewsModel
import com.nextreality.emptyrecyclerviewsample.ui.adapter.viewholder.generated.NewsItemViewHolderGenerated
import com.nextreality.emptyrecyclerviewsample.ui.adapter.wrapper.NewsItemWrapper
import kotlinx.android.synthetic.main.item_news_item.view.*

class NewsItemViewHolder(parent: ViewGroup) : NewsItemViewHolderGenerated(parent) {

    private lateinit var news: NewsModel

    override fun bindViewHolder(obj: Any) {
        news = (obj as NewsItemWrapper).obj
        itemView.textView_title.text = obj.obj.name
        itemView.textView_subtitle.text = obj.obj.subtitle
        when (recyclerTracker.longSelectionTracker?.isSelected(news.id)) {
            true -> itemView.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.purple_200))
            false -> itemView.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.white))
        }
    }

    override fun bindViewHolder(obj: Any, payloads: MutableList<Any>) {

        // Populate Views with data. (Always populate same properties on every view. ex: always populate default value on else)
    }

    fun getItemDetails(): ItemDetailsLookup.ItemDetails<Long> =
        object : ItemDetailsLookup.ItemDetails<Long>() {
            override fun getPosition(): Int = absoluteAdapterPosition
            override fun getSelectionKey(): Long = news.id
//            override fun inSelectionHotspot(e: MotionEvent): Boolean {
//                return true
//            }
        }

    companion object {

        const val TAG = "NewsItemViewHolder"
    }
}
