package com.nextreality.emptyrecyclerviewsample.ui.activity

import android.os.Bundle
import com.nextreality.emptyrecyclerviewsample.ui.activity.generated.MainActivityGenerated

class MainActivity : MainActivityGenerated() {
	
	override fun onCreate(savedInstanceState: Bundle?) {
	
		super.onCreate(savedInstanceState)
		if (savedInstanceState == null) {
			navigationHelper.navigateToSampleScreenFragment(this)
		}
	}
	
	override fun setup() {
		
	}
}
