package com.nextreality.emptyrecyclerviewsample.ui.fragment

import android.os.Bundle
import com.tandeminnovation.appbase.helper.BundleHelper
import com.nextreality.emptyrecyclerviewsample.ui.fragment.generated.SelectionScreenFragmentGenerated
import com.tandeminnovation.appbase.base.ApiCallActionBase.ApiCallActionType
import com.tandeminnovation.appbase.eventbus.event.OnErrorEvent
import com.nextreality.emptyrecyclerviewsample.R
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.empty_recyclerview.*
import kotlinx.android.synthetic.main.fragment_selection_screen.*

class SelectionScreenFragment : SelectionScreenFragmentGenerated() {
	
	override fun setup(savedInstanceState: Bundle?) {
		super.setup(savedInstanceState)
	}
	
	
	
	override fun setupRecyclerView() {
		super.setupRecyclerView()
		recyclerView.apply {
			layoutManager = LinearLayoutManager(requireContext())
//			addItemDecoration(MarginDecoration(requireContext(), R.dimen.space_item_decoration_margin))
				adapter = mAdapter
		}
	}
	
	companion object {
		
		const val TAG = "SelectionScreenFragment"
		
		/**
		* Use this factory method to create a new instance of this fragment using
		* the provided parameters.
		*/
		fun newInstance(): SelectionScreenFragment {
		
			return SelectionScreenFragment()
		}
	}
}
