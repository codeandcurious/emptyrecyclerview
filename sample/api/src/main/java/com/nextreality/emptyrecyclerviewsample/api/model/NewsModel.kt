package com.nextreality.emptyrecyclerviewsample.api.model

import android.os.Parcel
import android.os.Parcelable
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import kotlin.jvm.Throws
import com.tandeminnovation.appbase.extension.*
import com.nextreality.emptyrecyclerviewsample.api.model.generated.NewsModelGenerated;

class NewsModel : NewsModelGenerated {
	
	constructor() : super()
	
	@Throws(JSONException::class)
	constructor(json: JSONObject) : super() {
		this.fromJson(json)
	}
	
	constructor(`in`: Parcel) : super() {
		this.readFromParcel(`in`)
	}
	
	companion object {
	
		private val companion: NewsModelGenerated.Companion = NewsModelGenerated.Companion
		
		fun fromJsonObject(jsonObject: JSONObject): NewsModel {
		
			val result = NewsModel()
			result.fromJson(jsonObject)
			
			return result
		}
		
		fun toJsonObject(model: NewsModel): JSONObject {
		
			return model.toJson()
		}
		
		fun fromJsonArray(array: JSONArray?): MutableList<NewsModel> {
		
			return companion.fromJsonArray(array)
		}
		
		fun toJsonArray(models: MutableList<NewsModel>): JSONArray {
		
			return companion.toJsonArray(models)
		}
		
		@JvmField
		val CREATOR: Parcelable.Creator<NewsModel> = object : Parcelable.Creator<NewsModel> {
		
			override fun createFromParcel(`in`: Parcel): NewsModel {
			
				return NewsModel(`in`)
			}
			
			override fun newArray(size: Int): Array<NewsModel?> {
			
				return arrayOfNulls(size)
			}
		}
	}
}
