// Managed by geny -----------------------------------------------------------
// <auto-generated>
//		     This code was generated by Rui Milagaia's Geny Tool.
//		     Runtime Version: 1.2 
//
//		     Changes to this file may cause incorrect behavior and will be lost if
//		     the code is regenerated.
// </auto-generated>
// Managed by geny -----------------------------------------------------------

package com.nextreality.emptyrecyclerviewsample.api.model.generated

import android.os.Parcel
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import com.tandeminnovation.appbase.base.ModelBase
import com.tandeminnovation.appbase.helper.JsonHelper
import kotlin.jvm.Throws
import com.tandeminnovation.appbase.extension.*
import com.nextreality.emptyrecyclerviewsample.api.model.*

abstract class NewsModelGenerated : ModelBase() {
	
	// members
	var id: Long = 0
	var name: String? = null
	var subtitle: String? = null
	var content: String? = null
	
	@Throws(JSONException::class)
	override fun fromJson(json: JSONObject) {
		
		if (JsonHelper.hasKey(json, JSON_ID)) {
		
			this.id = JsonHelper.parseLong(json, JSON_ID)
		}
		
		if (JsonHelper.hasKey(json, JSON_NAME)) {
		
			this.name = JsonHelper.parseNullableString(json, JSON_NAME)
		}
		
		if (JsonHelper.hasKey(json, JSON_SUBTITLE)) {
		
			this.subtitle = JsonHelper.parseNullableString(json, JSON_SUBTITLE)
		}
		
		if (JsonHelper.hasKey(json, JSON_CONTENT)) {
		
			this.content = JsonHelper.parseNullableString(json, JSON_CONTENT)
		}
	}
	
	@Throws(JSONException::class)
	override fun toJson(): JSONObject {
		val obj = JSONObject()
		
		obj.put(JSON_ID, id?.let { JsonHelper.format(it) })
		
		obj?.put(JSON_NAME, name?.let { JsonHelper.format(it) })
		
		obj?.put(JSON_SUBTITLE, subtitle?.let { JsonHelper.format(it) })
		
		obj?.put(JSON_CONTENT, content?.let { JsonHelper.format(it) })
		
		return obj
	}
	
	override fun writeToParcel(dest: Parcel, flags: Int) {
	
		super.writeToParcel(dest, flags)
	}
	
	override fun readFromParcel(`in`: Parcel) {
	
		super.readFromParcel(`in`)
	}
	
	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (other !is NewsModelGenerated) return false
		if (id != other.id) return false
		if (name != other.name) return false
		if (subtitle != other.subtitle) return false
		if (content != other.content) return false
		return true
	}
	
	override fun hashCode(): Int {
		var result = 0
		result = 31 * result + id.hashCode()
		result = 31 * result + (name?.hashCode() ?: 0)
		result = 31 * result + (subtitle?.hashCode() ?: 0)
		result = 31 * result + (content?.hashCode() ?: 0)
		return result
	}
	
	companion object {
	
		// json attributes
		const val JSON_ID = "id"
		const val JSON_NAME = "name"
		const val JSON_SUBTITLE = "subtitle"
		const val JSON_CONTENT = "content"
		
		@Throws(JSONException::class)
		fun toJsonArray(models: MutableList<NewsModel>?): JSONArray {
			val result = JSONArray()
			if (models != null) {
			
				for (i in models.indices) {
				
					result.put(models[i].toJson())
				}
			}
			
			return result
		}
		
		@Throws(JSONException::class)
		fun fromJsonArray(array: JSONArray?): MutableList<NewsModel> {
		
			val models = mutableListOf<NewsModel>()
			if (array != null) {
			
				for (i in 0 until array.length()) {
				
					models.add(NewsModel(array.getJSONObject(i)))
				}
			}
			
			return models
		}
	}
}
