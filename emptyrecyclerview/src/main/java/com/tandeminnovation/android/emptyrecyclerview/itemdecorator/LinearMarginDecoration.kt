package com.tandeminnovation.android.emptyrecyclerview.itemdecorator

import android.graphics.Rect
import android.view.View
import androidx.annotation.Px
import androidx.recyclerview.widget.RecyclerView
import com.tandeminnovation.android.emptyrecyclerview.interfaces.AbstractMarginDecoration
import com.tandeminnovation.android.emptyrecyclerview.interfaces.PositionDecorationValidator

class LinearMarginDecoration(
    @Px private var leftMargin: Int = 0,
    @Px private var topMargin: Int = 0,
    @Px private var rightMargin: Int = 0,
    @Px private var bottomMargin: Int = 0,
    private var orientation: Int = RecyclerView.VERTICAL,
    private var inverted: Boolean = false,
    private var positionDecorationValidator: PositionDecorationValidator? = null
) : AbstractMarginDecoration(positionDecorationValidator) {

    constructor(@Px margin: Int = 0, orientation: Int = RecyclerView.VERTICAL, inverted: Boolean = false, positionDecorationValidator: PositionDecorationValidator? = null) : this(
        margin,
        margin,
        margin,
        margin,
        orientation,
        inverted,
        positionDecorationValidator
    )

    override fun getItemOffsets(outRect: Rect, view: View, position: Int, parent: RecyclerView, state: RecyclerView.State, layoutManager: RecyclerView.LayoutManager) {
        val itemCount = layoutManager.itemCount
        if (orientation == RecyclerView.VERTICAL) {
            applyVerticalOffsets(outRect, position, itemCount)
        } else {
            applyHorizontalOffsets(outRect, position, itemCount)
        }
    }

    private fun applyVerticalOffsets(outRect: Rect, position: Int, itemCount: Int) {
        if (position == 0) {
            if (!inverted) {
                if (position == itemCount - 1) {
                    outRect.bottom = bottomMargin
                } else {
                    outRect.bottom = bottomMargin / 2
                }
                outRect.top = topMargin
            } else {
                if (position == itemCount - 1) {
                    outRect.top = topMargin
                } else {
                    outRect.top = topMargin / 2
                }
                outRect.bottom = bottomMargin
            }
        } else if (position == itemCount - 1) {
            if (!inverted) {
                outRect.top = topMargin / 2
                outRect.bottom = bottomMargin
            } else {
                outRect.bottom = bottomMargin / 2
                outRect.top = topMargin
            }
        } else {
            outRect.top = topMargin / 2
            outRect.bottom = bottomMargin / 2
        }
        outRect.left = leftMargin
        outRect.right = rightMargin
    }

    private fun applyHorizontalOffsets(outRect: Rect, position: Int, itemCount: Int) {
        if (position == 0) {
            if (!inverted) {
                outRect.left = leftMargin
                outRect.right = rightMargin / 2
            } else {
                outRect.right = rightMargin
                outRect.left = leftMargin / 2
            }
        }
        if (position == itemCount - 1) {
            if (!inverted) {
                if (position != 0) {
                    outRect.left = leftMargin / 2
                }
                outRect.right = rightMargin
            } else {
                if (position != 0) {
                    outRect.right = rightMargin / 2
                }
                outRect.left = leftMargin
            }
        } else {
            outRect.left = leftMargin / 2
            outRect.right = rightMargin / 2
        }
        outRect.top = topMargin
        outRect.bottom = bottomMargin
    }

    companion object {

        @JvmStatic
        fun createVertical(
            @Px verticalMargin: Int,
            orientation: Int = RecyclerView.VERTICAL,
            inverted: Boolean = false,
            positionDecorationValidator: PositionDecorationValidator? = null
        ): LinearMarginDecoration {
            return LinearMarginDecoration(
                leftMargin = 0,
                rightMargin = 0,
                topMargin = verticalMargin,
                bottomMargin = verticalMargin,
                orientation = orientation,
                inverted = inverted,
                positionDecorationValidator = positionDecorationValidator
            )
        }

        @JvmStatic
        fun createHorizontal(
            @Px horizontalMargin: Int,
            orientation: Int = RecyclerView.HORIZONTAL,
            inverted: Boolean = false,
            positionDecorationValidator: PositionDecorationValidator? = null
        ): LinearMarginDecoration {
            return LinearMarginDecoration(
                leftMargin = horizontalMargin,
                rightMargin = horizontalMargin,
                topMargin = 0,
                bottomMargin = 0,
                orientation = orientation,
                inverted = inverted,
                positionDecorationValidator = positionDecorationValidator
            )
        }
    }

}