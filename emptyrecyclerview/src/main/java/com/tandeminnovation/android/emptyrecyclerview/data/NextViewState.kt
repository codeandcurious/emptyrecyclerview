package com.tandeminnovation.android.emptyrecyclerview.data

enum class NextViewState {
    Loading,
    Empty,
    Error,
    Loaded
}