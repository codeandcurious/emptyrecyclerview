package com.tandeminnovation.android.emptyrecyclerview.data

enum class ItemDividerContentType {
    Drawable,
    Paint
}