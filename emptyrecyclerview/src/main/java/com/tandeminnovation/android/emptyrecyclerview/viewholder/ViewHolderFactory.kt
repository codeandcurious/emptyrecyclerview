/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.android.emptyrecyclerview.viewholder

import android.annotation.SuppressLint
import android.view.ViewGroup
import java.util.*

/**
 * Created by Rui on 27-Sep-15.
 */
class ViewHolderFactory {

    @SuppressLint("UseSparseArrays")
    private val viewHolderBuilders = HashMap<Int, ViewHolderBuilder>()

    /**
     * Register view holder builder.
     *
     * @param viewType the view type
     * @param builder  the builder
     */
    fun registerViewHolderBuilder(viewType: Int, builder: ViewHolderBuilder) {

        viewHolderBuilders[viewType] = builder
    }

    /**
     * Create view holder view holder base.
     *
     * @param viewGroup the view group
     * @param viewType  the view type
     * @return the view holder base
     */
    fun createViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolderBase {

        val builder = viewHolderBuilders[viewType]
                ?: throw RuntimeException("Not found ViewHolder builder for viewType: $viewType")

        return builder.buildViewHolder(viewGroup)
    }
}
