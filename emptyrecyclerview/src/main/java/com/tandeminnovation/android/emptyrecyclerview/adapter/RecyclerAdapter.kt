/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.android.emptyrecyclerview.adapter

import android.view.ViewGroup
import androidx.recyclerview.selection.SelectionTracker
import com.tandeminnovation.android.emptyrecyclerview.data.ItemWrapper
import com.tandeminnovation.android.emptyrecyclerview.data.RecyclerTracker
import com.tandeminnovation.android.emptyrecyclerview.viewholder.ItemTypeCheck
import com.tandeminnovation.android.emptyrecyclerview.viewholder.ViewHolderBase
import com.tandeminnovation.android.emptyrecyclerview.viewholder.ViewHolderFactory

/**
 * Created by Rui on 27-Sep-15.
 */
open class RecyclerAdapter : androidx.recyclerview.widget.RecyclerView.Adapter<ViewHolderBase>() {

    private var factory: ViewHolderFactory? = null
    private var typeCheck: ItemTypeCheck? = null
    var recyclerTracker: RecyclerTracker = RecyclerTracker()


    /**
     * Gets data set.
     *
     * @return the data set
     */
    var dataSet: List<Any> = mutableListOf()
        private set

    /**
     * Sets view holder factory.
     *
     * @param factory the factory
     */
    fun setViewHolderFactory(factory: ViewHolderFactory) {

        this.factory = factory
    }

    /**
     * Sets item type check.
     *
     * @param typeCheck the type check
     */
    fun setItemTypeCheck(typeCheck: ItemTypeCheck) {

        this.typeCheck = typeCheck
    }

    /**
     * Update dataset.
     *
     * @param objList the obj list
     */
    fun updateDataSet(objList: List<Any>) {

        this.dataSet = objList
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderBase {
        // create a new view
        // return new SimpleContactViewHolder(parent);
        return factory!!.createViewHolder(parent, viewType)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: ViewHolderBase, position: Int) {

        val obj = dataSet!![position]

        if (obj is ItemWrapper<*>) {

            holder.bindViewHolder(obj)
            holder.recyclerTracker = recyclerTracker
        } else {

            holder.bindViewHolder(obj)
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount(): Int {

        return if (dataSet != null) dataSet!!.size else 0
    }

    override fun getItemViewType(position: Int): Int {

        val type: Int
        val obj = dataSet!![position]

        if (obj is ItemWrapper<*>) {

            type = obj.type
        } else {

            type = typeCheck!!.getType(obj)
        }

        return type
    }

    /**
     * Gets item.
     *
     * @param position the position
     * @return the item
     */
    fun getItem(position: Int): Any? {
        var result: Any? = null
        if (dataSet != null && !dataSet!!.isEmpty()) {

            result = dataSet!![position]
        }

        return result
    }

    companion object {

        /**
         * The constant TAG.
         */
        val TAG = RecyclerAdapter::class.java.canonicalName
    }
}
