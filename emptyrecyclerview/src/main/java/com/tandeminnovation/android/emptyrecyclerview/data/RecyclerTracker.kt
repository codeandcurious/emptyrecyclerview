package com.tandeminnovation.android.emptyrecyclerview.data

import androidx.recyclerview.selection.SelectionTracker

data class RecyclerTracker(val longSelectionTracker: SelectionTracker<Long>? = null, val stringSelectionTracker: SelectionTracker<String>? = null) {
}