package com.tandeminnovation.android.emptyrecyclerview.interfaces

fun interface PositionDecorationValidator {

    /**
     * Notifies about a position change depending on
     */
    fun doesPositionHaveDecoration(position: Int, itemCount: Int): Boolean
}