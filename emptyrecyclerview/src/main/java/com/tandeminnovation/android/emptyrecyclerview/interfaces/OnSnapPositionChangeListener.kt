package com.tandeminnovation.android.emptyrecyclerview.interfaces

fun interface OnSnapPositionChangeListener {

    /**
     * Notifies about a position change depending on
     */
    fun onSnapPositionChange(position: Int)
}