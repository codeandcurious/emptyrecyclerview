/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.android.emptyrecyclerview.viewholder

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.widget.RecyclerView
import com.tandeminnovation.android.emptyrecyclerview.data.RecyclerTracker

abstract class ViewHolderBase(parent: ViewGroup, layoutId: Int) : RecyclerView.ViewHolder(LayoutInflater.from(parent.context).inflate(layoutId, parent, false)) {

    val context: Context by lazy { itemView.context }
    var recyclerTracker: RecyclerTracker = RecyclerTracker()

    init {
        this.findViews()
    }

    open fun findViews() {

    }

    abstract fun bindViewHolder(obj: Any)

    abstract fun bindViewHolder(obj: Any, payloads: MutableList<Any>)

}
