/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.android.emptyrecyclerview.viewholder

import android.view.ViewGroup
import com.tandeminnovation.android.emptyrecyclerview.viewholder.ViewHolderBase

/**
 * Created by Rui on 27-Sep-15.
 */
interface ViewHolderBuilder {

    /**
     * Build view holder view holder base.
     *
     * @param parent the parent
     * @return the view holder base
     */
    fun buildViewHolder(parent: ViewGroup): ViewHolderBase
}
