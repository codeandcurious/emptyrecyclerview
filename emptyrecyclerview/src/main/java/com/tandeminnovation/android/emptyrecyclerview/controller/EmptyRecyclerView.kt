/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.android.emptyrecyclerview.controller

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.core.view.doOnLayout
import androidx.recyclerview.widget.RecyclerView
import com.tandeminnovation.android.emptyrecyclerview.R
import com.tandeminnovation.android.emptyrecyclerview.adapter.RecyclerAdapter
import com.tandeminnovation.android.emptyrecyclerview.data.NextViewState

/**
 * Created by firetrap on 26/06/2017.
 * Revamp by Skinnyy on 23/03/2021.
 */
class EmptyRecyclerView (
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : RecyclerView(context, attrs, defStyleAttr) {
    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, attrs: AttributeSet) : this(context, attrs, 0)

    private var viewState: NextViewState = NextViewState.Loading

    private var emptyView: View? = null
    private var errorView: View? = null
    private var loadingView: View? = null

    init {
        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.EmptyRecyclerView,
            0,
            0
        ).apply {
            try {
                val emptyViewId = getResourceId(R.styleable.EmptyRecyclerView_emptyView, -1)
                val errorViewId = getResourceId(R.styleable.EmptyRecyclerView_errorView, -1)
                val loadingViewId = getResourceId(R.styleable.EmptyRecyclerView_loadingView, -1)

                doOnLayout { recyclerView: View ->
                    (recyclerView.parent as View).apply {
                        if (emptyViewId != -1) {
                            this.findViewById<View>(emptyViewId)?.let { emptyView = it }
                        }
                        if (errorViewId != -1) {
                            this.findViewById<View>(errorViewId)?.let { errorView = it }
                        }
                        if (loadingViewId != -1) {
                            this.findViewById<View>(loadingViewId)?.let { loadingView = it }
                        }
                    }
                    viewState = NextViewState.Loading
                    handleViewsForState()
                }
            } finally {
                recycle()
            }
        }

    }

    private val dataObserver = object : RecyclerView.AdapterDataObserver() {
        override fun onChanged() {
            when {
                adapter != null && adapter?.itemCount != 0 -> setViewState(NextViewState.Loaded)
                adapter != null && adapter?.itemCount == 0 -> setViewState(NextViewState.Empty)
                else -> setViewState(NextViewState.Error)
            }
        }
    }

    override fun setAdapter(adapter: RecyclerView.Adapter<*>?) {
        super.setAdapter(adapter)
        adapter?.registerAdapterDataObserver(this.dataObserver)
    }

    private fun handleViewsForState() {
        when (viewState) {
            NextViewState.Loading -> {
                emptyView?.visibility = View.GONE
                errorView?.visibility = View.GONE
                loadingView?.visibility = View.VISIBLE
            }
            NextViewState.Empty -> {
                emptyView?.visibility = View.VISIBLE
                errorView?.visibility = View.GONE
                loadingView?.visibility = View.GONE
            }
            NextViewState.Error -> {
                emptyView?.visibility = View.GONE
                errorView?.visibility = View.VISIBLE
                loadingView?.visibility = View.GONE
            }
            NextViewState.Loaded -> {
                emptyView?.visibility = View.GONE
                errorView?.visibility = View.GONE
                loadingView?.visibility = View.GONE
            }
        }
    }

    fun setViewState(viewState: NextViewState) {
        this.viewState = viewState
        handleViewsForState()
    }

    fun clearDataAndShowViewState(viewState: NextViewState) {
        (adapter as RecyclerAdapter).updateDataSet(mutableListOf())
        adapter?.notifyDataSetChanged()
        setViewState(viewState)
    }
}