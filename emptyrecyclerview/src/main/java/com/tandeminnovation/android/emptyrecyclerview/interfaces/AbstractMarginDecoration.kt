package com.tandeminnovation.android.emptyrecyclerview.interfaces

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class AbstractMarginDecoration(private val positionDecorationValidator: PositionDecorationValidator? = null) : RecyclerView.ItemDecoration() {

    final override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        val lm = parent.layoutManager as RecyclerView.LayoutManager
        val layoutParams = view.layoutParams as RecyclerView.LayoutParams
        val position = layoutParams.absoluteAdapterPosition
        if (position != RecyclerView.NO_POSITION &&
            (positionDecorationValidator == null || positionDecorationValidator.doesPositionHaveDecoration(position, lm.itemCount))
        ) {
            getItemOffsets(outRect, view, position, parent, state, lm)
        }
    }

    abstract fun getItemOffsets(
        outRect: Rect,
        view: View,
        position: Int,
        parent: RecyclerView,
        state: RecyclerView.State,
        layoutManager: RecyclerView.LayoutManager
    )
}