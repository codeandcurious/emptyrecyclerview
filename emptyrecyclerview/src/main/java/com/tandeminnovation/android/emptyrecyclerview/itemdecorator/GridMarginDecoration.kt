package com.tandeminnovation.android.emptyrecyclerview.itemdecorator

import android.graphics.Rect
import android.view.View
import androidx.annotation.Px
import androidx.recyclerview.widget.RecyclerView
import com.tandeminnovation.android.emptyrecyclerview.interfaces.AbstractMarginDecoration
import com.tandeminnovation.android.emptyrecyclerview.interfaces.PositionDecorationValidator
import kotlin.math.ceil

class GridMarginDecoration(
    @Px private var horizontalMargin: Int,
    @Px private var verticalMargin: Int,
    private var numberOfColumns: Int,
    private var orientation: Int = RecyclerView.VERTICAL,
    private var inverted: Boolean = false,
    private var positionDecorationValidator: PositionDecorationValidator? = null
) : AbstractMarginDecoration(positionDecorationValidator) {

    constructor(
        @Px margin: Int,
        numberOfColumns: Int,
        orientation: Int = RecyclerView.VERTICAL,
        inverted: Boolean = false,
        positionDecorationValidator: PositionDecorationValidator? = null
    ) : this(margin, margin, numberOfColumns, orientation, inverted, positionDecorationValidator)

    companion object {

        @JvmStatic
        fun createVertical(
            @Px verticalMargin: Int,
            numberOfColumns: Int,
            orientation: Int = RecyclerView.VERTICAL,
            inverted: Boolean = false,
            positionDecorationValidator: PositionDecorationValidator? = null
        ): GridMarginDecoration {
            return GridMarginDecoration(
                verticalMargin = verticalMargin,
                horizontalMargin = 0,
                numberOfColumns = numberOfColumns,
                orientation = orientation,
                inverted = inverted,
                positionDecorationValidator = positionDecorationValidator
            )
        }

        @JvmStatic
        fun createHorizontal(
            @Px horizontalMargin: Int,
            numberOfColumns: Int,
            orientation: Int = RecyclerView.VERTICAL,
            inverted: Boolean = false,
            positionDecorationValidator: PositionDecorationValidator? = null
        ): GridMarginDecoration {
            return GridMarginDecoration(
                horizontalMargin = horizontalMargin,
                verticalMargin = 0,
                numberOfColumns = numberOfColumns,
                orientation = orientation,
                inverted = inverted,
                positionDecorationValidator = positionDecorationValidator
            )
        }
    }

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        position: Int,
        parent: RecyclerView,
        state: RecyclerView.State,
        layoutManager: RecyclerView.LayoutManager
    ) {
        val columnIndex = position.rem(numberOfColumns)

        val lines = ceil(layoutManager.itemCount / numberOfColumns.toDouble()).toInt()
        val lineIndex = position / numberOfColumns

        if (orientation == RecyclerView.VERTICAL) {
            applyVerticalOffsets(outRect, numberOfColumns, columnIndex, lines, lineIndex)
        } else {
            applyHorizontalOffsets(outRect, numberOfColumns, columnIndex, lines, lineIndex)
        }
    }

    private fun applyVerticalOffsets(
        outRect: Rect,
        numberOfColumns: Int,
        columnIndex: Int,
        lines: Int,
        lineIndex: Int
    ) {
        val startPadding = horizontalMargin * ((numberOfColumns - columnIndex) / numberOfColumns.toFloat())
        val endPadding = horizontalMargin * ((columnIndex + 1) / numberOfColumns.toFloat())
        outRect.left = startPadding.toInt()
        outRect.right = endPadding.toInt()

        if (lineIndex == 0) {
            if (!inverted) {
                outRect.top = verticalMargin
                if (lines > 1) {
                    outRect.bottom = verticalMargin / 2
                } else {
                    outRect.bottom = verticalMargin
                }
            } else {
                outRect.bottom = verticalMargin
                if (lines > 1) {
                    outRect.top = verticalMargin / 2
                } else {
                    outRect.top = verticalMargin
                }
            }
        } else if (lineIndex == lines - 1) {
            if (!inverted) {
                outRect.top = verticalMargin / 2
                outRect.bottom = verticalMargin
            } else {
                outRect.top = verticalMargin
                outRect.bottom = verticalMargin / 2
            }
        } else {
            outRect.top = verticalMargin / 2
            outRect.bottom = verticalMargin / 2
        }
    }

    private fun applyHorizontalOffsets(
        outRect: Rect,
        numberOfColumns: Int,
        columnIndex: Int,
        lines: Int,
        lineIndex: Int
    ) {
        val startPadding = verticalMargin * ((numberOfColumns - columnIndex) / numberOfColumns.toFloat())
        val endPadding = verticalMargin * ((columnIndex + 1) / numberOfColumns.toFloat())

        outRect.top = startPadding.toInt()
        outRect.bottom = endPadding.toInt()

        if (lineIndex == 0) {
            if (!inverted) {
                outRect.left = horizontalMargin
                if (lines > 1) {
                    outRect.right = horizontalMargin / 2
                } else {
                    outRect.right = horizontalMargin
                }
            } else {
                outRect.right = horizontalMargin
                if (lines > 1) {
                    outRect.left = horizontalMargin / 2
                } else {
                    outRect.left = horizontalMargin
                }
            }
        } else if (lineIndex == lines - 1) {
            if (!inverted) {
                outRect.left = horizontalMargin / 2
                outRect.right = horizontalMargin
            } else {
                outRect.left = horizontalMargin
                outRect.right = horizontalMargin / 2
            }
        } else {
            outRect.left = horizontalMargin / 2
            outRect.right = horizontalMargin / 2
        }
    }

}