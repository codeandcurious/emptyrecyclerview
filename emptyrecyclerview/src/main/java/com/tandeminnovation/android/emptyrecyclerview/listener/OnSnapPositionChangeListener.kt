package com.tandeminnovation.android.emptyrecyclerview.listener

fun interface OnSnapPositionChangeListener {

    /**
     * Notifies about a position change depending on
     */
    fun onSnapPositionChange(position: Int)
}